//
//  SampleApp.swift
//  Sample
//
//  Created by Yuya Oka on 2021/08/13.
//

import SwiftUI

@main
struct SampleApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
        }
    }
}
