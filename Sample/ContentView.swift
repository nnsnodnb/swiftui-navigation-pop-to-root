//
//  ContentView.swift
//  Sample
//
//  Created by Yuya Oka on 2021/08/13.
//

import SwiftUI

struct ContentView: View {

    @State var isActive = false

    var body: some View {
        VStack {
            Spacer()
            Text("This is 1st view")
                .font(.title)
                .fontWeight(.black)
            Spacer()
            NavigationLink(
                destination: SecondContentView(isActive: $isActive),
                isActive: $isActive,
                label: {
                    EmptyView()
                })
            Button(action: {
                isActive = true
            }, label: {
                Text("Go to 2nd view")
                    .foregroundColor(.white)
                    .fontWeight(.medium)
                    .padding(.horizontal, 60)
                    .frame(height: 50)
                    .background(Color.black)
                    .cornerRadius(25)
            })
        }
        .navigationBarHidden(true)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
