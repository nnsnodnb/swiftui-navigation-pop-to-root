//
//  ThirdContentView.swift
//  Sample
//
//  Created by Yuya Oka on 2021/08/13.
//

import SwiftUI

struct ThirdContentView: View {

    @Binding var isActive: Bool

    var body: some View {
        ZStack {
            Color.purple
                .ignoresSafeArea()
            VStack {
                Spacer()
                Text("This is 3rd view")
                    .font(.title)
                    .fontWeight(.black)
                    .foregroundColor(.white)
                Spacer()
                Button(action: {
                    isActive = false
                }, label: {
                    Text("Back to top")
                        .foregroundColor(.purple)
                })
                .padding(.horizontal, 60)
                .frame(height: 50)
                .background(Color.white)
                .cornerRadius(25)
            }
        }
        .navigationBarHidden(true)
    }
}

struct ThirdContentView_Previews: PreviewProvider {
    static var previews: some View {
        ThirdContentView(isActive: .constant(true))
    }
}
