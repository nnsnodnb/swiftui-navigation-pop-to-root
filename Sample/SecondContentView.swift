//
//  SecondContentView.swift
//  Sample
//
//  Created by Yuya Oka on 2021/08/13.
//

import SwiftUI

struct SecondContentView: View {

    @Binding var isActive: Bool

    var body: some View {
        ZStack {
            Color.orange
                .ignoresSafeArea()
            VStack {
                Spacer()
                Text("This is 2nd View")
                    .font(.title)
                    .fontWeight(.black)
                    .foregroundColor(.white)
                Spacer()
                NavigationLink(
                    destination: ThirdContentView(isActive: $isActive),
                    label: {
                        Text("Go to 3rd view")
                            .foregroundColor(.orange)
                            .fontWeight(.medium)
                            .padding(.horizontal, 60)
                            .frame(height: 50)
                            .background(Color.white)
                            .cornerRadius(25)
                    })
            }
        }
        .navigationBarHidden(true)
    }
}

struct SecondContentView_Previews: PreviewProvider {
    static var previews: some View {
        SecondContentView(isActive: .constant(true))
    }
}
